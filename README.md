# Smart Door
## Overview
This is part of a research project, done in Ambient Intelligence lab at Sharif University of Technology. We are implementing a Cloud-assisted IoT based system to boost student-professor interaction. This is the android client side that students use it to communicate with smart door. Each door has a unique identity which is saved in QrCode and NFC tags installed on the door. The project's server side uses Parse and its libraries.

## System Architecture
![arch.png](https://bitbucket.org/repo/Xdzgje/images/697768544-arch.png)

## Work Flow
![sc.png](https://bitbucket.org/repo/Xdzgje/images/984727696-sc.png)

## License
Copyright (c) 2014, Parse, LLC. All rights reserved.

You are hereby granted a non-exclusive, worldwide, royalty-free license to use,
copy, modify, and distribute this software in source code or binary form for use
in connection with the web services and APIs provided by Parse.

As with any software that integrates with the Parse platform, your use of
this software is subject to the Parse Terms of Service
[https://www.parse.com/about/terms]. This copyright notice shall be
included in all copies or substantial portions of the software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.