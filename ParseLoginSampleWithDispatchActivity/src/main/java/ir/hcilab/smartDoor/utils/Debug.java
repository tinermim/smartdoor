package ir.hcilab.smartDoor.utils;

import android.util.Log;

/**
 * Created by ramtin on 9/15/15 AD.
 */
public class Debug {
  public static final boolean LOG = true;

  public static void Log(String tag, String msg) {
    if (LOG) Log.i(tag, msg);
  }
}
