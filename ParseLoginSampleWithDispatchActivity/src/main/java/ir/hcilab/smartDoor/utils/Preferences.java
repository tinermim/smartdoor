package ir.hcilab.smartDoor.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import ir.hcilab.smartDoor.report.Door;
import ir.hcilab.smartDoor.report.Message;

/**
 * Created by ramtin on 11/23/15 AD.
 */
public final class Preferences {
    public final static String DOOR_KEY = "DOOR_KEY";
    public final static String MESSAGE_KEY = "MESSAGE_KEY";
    public static SharedPreferences prefs;

    public static void setPrefs(Context context) {
        if (prefs == null)
            prefs = context.getSharedPreferences("door", Context.MODE_ENABLE_WRITE_AHEAD_LOGGING);
    }

    public static void addDoor(Context context, Door door) {
        List<Door> doors = getDoors(context);
        if (!doors.contains(door)) {
            doors.add(door);
        } else {
            doors.get(doors.indexOf(door)).date = door.date;
        }
        String json = new Gson().toJson(doors);
        prefs.edit().putString(DOOR_KEY, json).apply();
    }

    public static void deleteDoor(Context context, Door door) {
        List<Door> doors = getDoors(context);
        for (int i = 0; i < doors.size(); i++) {
            if (doors.get(i).equals(door)) {
                Log.d("delete", "door deleted: " + doors.get(i).text);
                doors.remove(i);
                break;
            }
        }

        List<Message> message = getMessages(context);
        for (int i = 0; i < message.size(); i++) {
            if (message.get(i).doorName.equals(door.text)) {
                Log.d("delete", "message deleted: " + message.get(i).text);
                message.remove(i);
            }
        }

        String jsonDoor = new Gson().toJson(doors);
        String jsonMessage = new Gson().toJson(message);

        prefs.edit().putString(DOOR_KEY, jsonDoor).apply();
        prefs.edit().putString(MESSAGE_KEY, jsonMessage).apply();
    }

    public static List<Door> getDoors(Context context) {
        setPrefs(context);
        String json = prefs.getString(DOOR_KEY, "");
        Type type = new TypeToken<List<Door>>() {
        }.getType();
        List<Door> inpList = new Gson().fromJson(json, type);
        if (inpList == null) inpList = new ArrayList<>();
        return inpList;
    }

    public static void addMessage(Context context, Message message) {
        List<Message> messages = getMessages(context);
        if (!messages.contains(message)) {
            messages.add(message);
            int firstIndex = -1;
            int doorMessageNum = 0;
            for (int i = 0; i < messages.size(); i++) {
                Debug.Log("Messages", messages.get(i).doorName + " " + messages.get(i).sender);
                if (messages.get(i).doorName.equals(message.doorName)) {
                    if (firstIndex == -1) firstIndex = i;
                    doorMessageNum++;
                }
            }
            if (doorMessageNum > 20) messages.remove(firstIndex);

            String json = new Gson().toJson(messages);
            prefs.edit().putString(MESSAGE_KEY, json).apply();
        }
    }

    public static List<Message> getMessages(Context context) {
        setPrefs(context);
        String json = prefs.getString(MESSAGE_KEY, "");
        Type type = new TypeToken<List<Message>>() {
        }.getType();
        List<Message> inpList = new Gson().fromJson(json, type);
        if (inpList == null) inpList = new ArrayList<>();
        return inpList;
    }

    public static List<Message> getMessages(Context context, String doorName) {
        setPrefs(context);
        String json = prefs.getString(MESSAGE_KEY, "");
        Type type = new TypeToken<List<Message>>() {
        }.getType();
        List<Message> inpList = new Gson().fromJson(json, type);
        if (inpList == null) inpList = new ArrayList<>();

        List<Message> messages = new ArrayList<Message>();

        for (int i = 0; i < inpList.size(); i++) {
            if (inpList.get(i).doorName.equals(doorName)) {
                messages.add(inpList.get(i));
            }
        }

        return messages;
    }
}
