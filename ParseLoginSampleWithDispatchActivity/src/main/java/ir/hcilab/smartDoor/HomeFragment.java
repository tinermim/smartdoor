package ir.hcilab.smartDoor;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import butterknife.Bind;
import butterknife.ButterKnife;
import ir.hcilab.smartDoor.nfc.NfcActivity;
import ir.hcilab.smartDoor.qr.QrCodeReaderActivity;

/**
 * Created by ramtin on 8/26/15 AD.
 * Home Fragment, this Fragment has to main buttons: NFC and QrCodeReader
 * Clicking on each button, user is directed to the target fragment. One to process the qrcode and
 * the other one to handle nfc communications with the door.
 */
public class HomeFragment extends Fragment {
  @Bind(R.id.qrCodeButton) ImageButton qrButton;
  @Bind(R.id.qrCodeContainer) LinearLayout qrContainer;

  @Bind(R.id.nfcButton) ImageButton nfcButton;
  @Bind(R.id.nfcConnectionContainer) LinearLayout nfcContainer;

  private static final int ANIMATION_DURATION = 700;

  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View mainView = inflater.inflate(R.layout.fragment_home, container, false);
    ButterKnife.bind(this, mainView);

    addListenerToImageButton(qrButton, qrContainer, QrCodeReaderActivity.class);
    addListenerToImageButton(nfcButton, nfcContainer, NfcActivity.class);

    return mainView;
  }

  private void addListenerToImageButton(ImageButton button, final View container,
      final Class target) {

    button.setOnTouchListener(new View.OnTouchListener() {
      @Override public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN
            || event.getAction() == MotionEvent.ACTION_MOVE) {
          v.setAlpha(.5f);
        } else {
          v.setAlpha(1f);
        }
        return false;
      }
    });

    button.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        YoYo.with(Techniques.RubberBand).duration(ANIMATION_DURATION).playOn(container);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
          @Override public void run() {
              Intent intent = new Intent(getActivity(), target);
              startActivity(intent);
          }
        }, ANIMATION_DURATION);
      }
    });
  }
}