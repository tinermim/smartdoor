package ir.hcilab.smartDoor.introduction;

import android.support.v4.view.ViewPager;

/**
 *
 * Paging indicator widgets that are compatible with the ViewPager from the Android Support Library
 * to improve discoverability of content.
 * 1. Include one of the widgets in your view. This should usually be placed adjacent to the ViewPager it represents.
 * 2. In your onCreate method (or onCreateView for a fragment), bind the indicator to the ViewPager.
 * 3. (Optional) If you use an OnPageChangeListener with your view pager you should set it in the indicator rather
 * than on the pager directly.
 */
interface PageIndicator extends ViewPager.OnPageChangeListener {
  /**
   * Bind the indicator to a ViewPager.
   *
   * @param view
   */
  void setViewPager(ViewPager view);

  /**
   * Bind the indicator to a ViewPager.
   *
   * @param view
   * @param initialPosition
   */
  void setViewPager(ViewPager view, int initialPosition);

  /**
   * <p>Set the current page of both the ViewPager and indicator.</p>
   *
   * <p>This <strong>must</strong> be used if you need to set the page before
   * the views are drawn on screen (e.g., default start page).</p>
   *
   * @param item
   */
  void setCurrentItem(int item);

  /**
   * Set a page change listener which will receive forwarded events.
   *
   * @param listener
   */
  void setOnPageChangeListener(ViewPager.OnPageChangeListener listener);

  /**
   * Notify the indicator that the fragment list has changed.
   */
  void notifyDataSetChanged();
}