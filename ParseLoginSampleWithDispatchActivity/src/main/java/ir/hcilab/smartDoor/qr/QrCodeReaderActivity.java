package ir.hcilab.smartDoor.qr;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.analytics.Tracker;
import com.google.zxing.Result;

import ir.hcilab.smartDoor.SmartDoorApplication;
import ir.hcilab.smartDoor.nfc.NfcActivity;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by ramtin
 */
public class QrCodeReaderActivity extends AppCompatActivity
    implements ZXingScannerView.ResultHandler {
  public static final String DOOR_ID = "doorId";
  private ZXingScannerView mScannerView;
  private final static String TAG = "QrCodeReader";
  Tracker mTracker;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mScannerView = new ZXingScannerView(this);
    setContentView(mScannerView);
    SmartDoorApplication application = (SmartDoorApplication) getApplication();
    mTracker = application.getGoogleAnalyticsTracker();
    mTracker.setScreenName("Qr Activity");
  }

  @Override public void onResume() {
    super.onResume();
    mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
    mScannerView.setAutoFocus(true);
    mScannerView.startCamera();          // Start camera on resume
  }

  @Override public void onPause() {
    super.onPause();
    mScannerView.stopCamera();           // Stop camera on pause
  }

  @Override public void handleResult(Result rawResult) {
    Intent i = new Intent(this, NfcActivity.class);
    i.putExtra(DOOR_ID, rawResult.getText());
    startActivity(i);
  }
}
