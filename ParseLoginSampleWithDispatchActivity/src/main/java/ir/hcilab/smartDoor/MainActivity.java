/*
 *  Copyright (c) 2015, Ambient Intelligence Lab, Sharif University of Technology. All rights reserved.
 *
 *  You are hereby granted a non-exclusive, worldwide, royalty-free license to use,
 *  copy, modify, and distribute this software in source code or binary form for use
 *  in connection with the web services and APIs provided by Parse.
 *
 *  As with any software that integrates with the Parse platform, your use of
 *  this software is subject to the Parse Terms of Service
 *  [https://www.parse.com/about/terms]. This copyright notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 *  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 *  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 *  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package ir.hcilab.smartDoor;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;

import com.google.android.gms.analytics.Tracker;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.parse.ParseAnalytics;
import com.parse.ParseUser;

import butterknife.Bind;
import butterknife.ButterKnife;
import ir.hcilab.smartDoor.introduction.Introduction;
import ir.hcilab.smartDoor.report.core.BaseActivity;

/**
 * Shows the user profile. This activity can only function when there is a valid user, so we must
 * protect it with {@link DispatchLoginActivity} in AndroidManifest.xml.
 */
public class MainActivity extends AppCompatActivity {
    @SuppressWarnings("unused")
    private Drawer mDrawer;
    @SuppressWarnings("unused")
    private ActionBarDrawerToggle mDrawerToggle;
    /**
     * main Layout of the Activity. not localized for future purposes.
     */
    private DrawerLayout mDrawerLayout;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    Tracker mTracker;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ParseAnalytics.trackAppOpenedInBackground(getIntent());

        SmartDoorApplication application = (SmartDoorApplication) getApplication();
        mTracker = application.getGoogleAnalyticsTracker();
        mTracker.setScreenName("Main Activity");

        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        //noinspection ConstantConditions
        getSupportActionBar().setTitle(null);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        //FIXME for older apis than 15, we should use setBackgroundDrawable
        mDrawerLayout.setBackground(null);

        initDrawer();
    }

    /**
     * responsible for creating the left drawer. Currently it has these items: 1. Home 2. Reports 3.
     * Introduction 4. Logout
     */
    @SuppressWarnings("deprecation")
    private void initDrawer() {
        ProfileDrawerItem profile;

        profile = new ProfileDrawerItem().withEmail(ParseUser.getCurrentUser().getEmail())
                .withIcon(ContextCompat.getDrawable(this, R.drawable.logo))
                .withName((String) ParseUser.getCurrentUser().get("name"));

        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder().withActivity(this)
                .withHeaderBackground(ContextCompat.getDrawable(this, R.drawable.drawer_header))
                .addProfiles(profile)
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();

        mDrawer = new DrawerBuilder().withActivity(this)
                .withToolbar(toolbar)
                .withCloseOnClick(true)
                .withAccountHeader(headerResult)
                .addDrawerItems(new PrimaryDrawerItem().withName(getString(R.string.Home))
                                .withIcon(GoogleMaterial.Icon.gmd_home)
                                .withSelectedColor(
                                        getResources().getColor(R.color.com_parse_ui_twitter_login_button))
                                .withSelectedIconColor(
                                        getResources().getColor(R.color.com_parse_ui_facebook_login_button_focus))
                                .withSelectedTextColor(
                                        getResources().getColor(R.color.com_parse_ui_facebook_login_button_focus))/*,
            new SecondaryDrawerItem().withName(getString(R.string.Settings))
                .withIcon(GoogleMaterial.Icon.gmd_settings)
                .withSelectedColor(getResources().getColor(
                    R.color.com_parse_ui_twitter_login_button))
                .withSelectedIconColor(
                    getResources().getColor(R.color.com_parse_ui_facebook_login_button_focus))
                .withSelectedTextColor(
                    getResources().getColor(R.color.com_parse_ui_facebook_login_button_focus))*/,
                        new SecondaryDrawerItem().withName(getString(R.string.Reports))
                                .withIcon(GoogleMaterial.Icon.gmd_history)
                                .withSelectedColor(
                                        getResources().getColor(R.color.com_parse_ui_twitter_login_button))
                                .withSelectedIconColor(
                                        getResources().getColor(R.color.com_parse_ui_facebook_login_button_focus))
                                .withSelectedIconColor(
                                        getResources().getColor(R.color.com_parse_ui_facebook_login_button_focus))
                                .withSelectedTextColor(
                                        getResources().getColor(R.color.com_parse_ui_facebook_login_button_focus)),
                        new SecondaryDrawerItem().withName(getString(R.string.Introduction))
                                .withIcon(GoogleMaterial.Icon.gmd_live_help)
                                .withSelectedColor(
                                        getResources().getColor(R.color.com_parse_ui_twitter_login_button))
                                .withSelectedIconColor(
                                        getResources().getColor(R.color.com_parse_ui_facebook_login_button_focus))
                                .withSelectedTextColor(
                                        getResources().getColor(R.color.com_parse_ui_facebook_login_button_focus)),
                        new SecondaryDrawerItem().withName(getString(R.string.Logout))
                                .withIcon(GoogleMaterial.Icon.gmd_lock)
                                .withSelectedColor(
                                        getResources().getColor(R.color.com_parse_ui_twitter_login_button))
                                .withSelectedTextColor(
                                        getResources().getColor(R.color.com_parse_ui_facebook_login_button_focus))
                                .withSelectedIconColor(
                                        getResources().getColor(R.color.com_parse_ui_facebook_login_button_focus)))
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(AdapterView<?> parent, View view, int position, long id,
                                               IDrawerItem drawerItem) {
                        if (id == 0) {
                            replaceFragment(HomeFragment.class);
                        }/* else if (id == 1) {
              replaceFragment(Settings.class);
            } */ else if (id == 1) {
                            Intent myIntent = new Intent(MainActivity.this, BaseActivity.class);
                            startActivity(myIntent);
                        } else if (id == 2) {
                            Intent myIntent = new Intent(MainActivity.this, Introduction.class);
                            startActivity(myIntent);
                        } else {
                            ParseUser.logOutInBackground();

                            // do something with the clicked item :D
                            // FLAG_ACTIVITY_CLEAR_TASK only works on API 11, so if the user
                            // logs out on older devices, we'll just exit.
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                Intent intent = new Intent(MainActivity.this, DispatchLoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            } else {
                                finish();
                            }
                        }
                        return false;
                    }
                })
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Set up the profile page based on the current user.
        ParseUser user = ParseUser.getCurrentUser();
        //showProfile(user);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        ParseUser.logOutInBackground();
        super.onDestroy();
    }

    /**
     * Shows the profile of the given user.
     */
    private void showProfile(ParseUser user) {
        if (user != null) {
            String fullName = user.getString("name");
        }
    }

    /**
     * Replace whatever is in the fragment_container view with this fragment and add the transaction
     * to the back stack.
     *
     * @param fragmentClass target class of the fragment replacing the old one
     */
    @SuppressWarnings("TryWithIdenticalCatches")
    public void replaceFragment(Class fragmentClass) {
        // Create new fragment and transaction
        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }
}
