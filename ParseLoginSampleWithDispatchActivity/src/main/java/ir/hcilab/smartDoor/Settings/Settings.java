package ir.hcilab.smartDoor.Settings;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import ir.hcilab.smartDoor.R;

/**
 * Created by ramtin on 8/26/15 AD.
 * Home Fragment, this Fragment has to main buttons: NFC and QrCodeReader
 * Clicking on each button, user is directed to the target fragment. One to process the qrcode and
 * the other one to handle nfc communications with the door.
 */
public class Settings extends Fragment {

  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View mainView = inflater.inflate(R.layout.fragment_settings, container, false);

    return mainView;
  }
}