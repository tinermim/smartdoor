package ir.hcilab.smartDoor.nfc;


import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.nfc.FormatException;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.NfcF;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.Tracker;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseUser;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.hcilab.smartDoor.R;
import ir.hcilab.smartDoor.SmartDoorApplication;
import ir.hcilab.smartDoor.qr.QrCodeReaderActivity;
import ir.hcilab.smartDoor.report.Door;
import ir.hcilab.smartDoor.report.Message;
import ir.hcilab.smartDoor.server.parse.ParseApiAdapter;
import ir.hcilab.smartDoor.utils.Debug;
import ir.hcilab.smartDoor.utils.Preferences;

/**
 * Near Field Communication (NFC) is a set of short-range wireless technologies, typically requiring
 * a distance of 4cm or less to initiate a connection. NFC allows you to share small payloads of
 * data between an NFC tag and an Android-powered device, or between two Android-powered devices.
 * Tags can range in complexity. Simple tags offer just read and write semantics, sometimes with
 * one-time-programmable areas to make the card read-only. More complex tags offer math operations,
 * and have cryptographic hardware to authenticate access to a sector. The most sophisticated tags
 * contain operating environments, allowing complex interactions with code executing on the tag. The
 * data stored in the tag can also be written in a variety of formats, but many of the Android
 * framework APIs are based around a NFC Forum standard called NDEF (NFC Data Exchange Format).
 * <p/>
 * Android-powered devices with NFC simultaneously support three main modes of operation: 1.
 * Reader/writer mode, allowing the NFC device to read and/or write passive NFC tags and stickers.
 * 2. P2P mode, allowing the NFC device to exchange data with other NFC peers; this operation mode
 * is used by Android Beam. 3. Card emulation mode, allowing the NFC device itself to act as an NFC
 * card. The emulated NFC card can then be accessed by an external NFC reader, such as an NFC
 * point-of-sale terminal. for more information you can go to this link:
 * http://developer.android.com/guide/topics/connectivity/nfc/nfc.html
 * <p/>
 * This class is responsible for reading the tag and get the door identity which is used for getting
 * the information from the cloud. At this moment we only support NDEF. But generally we could
 * handle all technologies mentioned in xml file stored in asset file of the project.
 */
public class NfcActivity extends AppCompatActivity {
    public static final String TAG = "NFC Intent";

    /**
     * cloud function names. These are the names of Parse cloud functions which we implemented in
     * Parse server and we can call them whenever we want relevant information.
     */
    private static final String CLOUD_FUNC_GET_PROFESSOR_MESSAGE = "getProfessorMessage";
    private static final String CLOUD_FUNC_SUBMIT_REPLY = "submitReply";
    private static final String CLOUD_FUNC_SUBMIT_PRESENCE = "submitPresence";

    private static final String CLOUD_PARAMETER_DOOR_ID = "door_id";
    private static final String CLOUD_PARAMETER_MESSAGE = "message";

    private NfcAdapter mAdapter;
    private PendingIntent mPendingIntent;
    private IntentFilter[] mFilters;
    private String[][] mTechLists;
    private String mDoorDbName = "";
    private String mDoorUID = "";

    private boolean isInSubmitMode = false;

    @Bind(R.id.tag_info)
    TextView tagInfo;
    @Bind(R.id.progressBar)
    ProgressBar progressBar;
    @Bind(R.id.submitPresenceProgressBar)
    ProgressBar submitPresenceProgressBar;
    @Bind(R.id.btnSubmitPresence)
    Button btnSubmitPresence;
    @Bind(R.id.replyMessageText)
    EditText replyMessageText;
    @Bind(R.id.btnReplyMessage)
    Button btnReplyMessage;
    @Bind(R.id.nfcTitle)
    TextView nfcTitle;
    Tracker mTracker;

    private String approach;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc);
        setTitle(null);
        ButterKnife.bind(this);
        tagInfo.setMovementMethod(new ScrollingMovementMethod());
        Typeface type = Typeface.createFromAsset(getAssets(), "Alegreya-Regular.otf");
        tagInfo.setTypeface(type);

        SmartDoorApplication application = (SmartDoorApplication) getApplication();
        mTracker = application.getGoogleAnalyticsTracker();
        mTracker.setScreenName("NFC Activity");

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Log.e(TAG, "extra not null...");
            approach = "QrCode";
            mDoorDbName = extras.getString(QrCodeReaderActivity.DOOR_ID);
            getMessageFromServer();
        } else {
            approach = "NFC";
            enableNfcIntents();
        }
    }

    @Override
    protected void onResume() {
        checkNfcEnable();
        super.onResume();
        if (mAdapter != null) {
            mAdapter.enableForegroundDispatch(this, mPendingIntent, mFilters, mTechLists);
        } else {
            enableNfcIntents();
        }
    }

    private void checkNfcEnable() {
        android.nfc.NfcAdapter mNfcAdapter = android.nfc.NfcAdapter.getDefaultAdapter(getApplicationContext());

        if (!mNfcAdapter.isEnabled()) {
            AlertDialog.Builder alertBox = new AlertDialog.Builder(this);

            LayoutInflater inflater = (LayoutInflater)getApplicationContext().getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            LinearLayout v = (LinearLayout) inflater.inflate(R.layout.dialog_nfc_enable, null);
            v.findViewById(R.id.confirm).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        Intent intent = new Intent(Settings.ACTION_NFC_SETTINGS);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                        startActivity(intent);
                    }
                    finish();
                }
            });

            v.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            alertBox.setView(v).setCancelable(false);
            alertBox.show();
        }

    }

    @Override
    protected void onPause() {
        if (mAdapter != null) mAdapter.disableForegroundDispatch(this);
        super.onPause();
    }

    /**
     * Create a generic PendingIntent that will be deliver to this activity. The NFC stack will fill
     * in the intent with the details of the discovered tag before delivering to this activity.
     */
    private void enableNfcIntents() {
        mAdapter = NfcAdapter.getDefaultAdapter(this);
        mPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        // Setup an intent filter for all MIME based dispatches
        IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        IntentFilter tech = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
        IntentFilter tag = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        //IntentFilter state = new IntentFilter(NfcAdapter.ACTION_ADAPTER_STATE_CHANGED);

        try {
            ndef.addDataType("*/*");
        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("fail", e);
        }
        mFilters = new IntentFilter[]{
                ndef, tech, tag/*, state*/
        };
        // Setup a tech list for all NfcF tags
        mTechLists = new String[][]{new String[]{NfcF.class.getName()}};
    }

    /**
     * submit the student's presence for this specific door. Later professor is able to check this.
     */
    @OnClick(R.id.btnSubmitPresence)
    public void submitPresence(View view) {
        submitPresenceProgressBar.setVisibility(View.VISIBLE);
        ParseApiAdapter.submitPresence(new ParseApiAdapter.Callback() {
            @Override
            public void error(ParseException e) {
                Toast.makeText(NfcActivity.this, e + "", Toast.LENGTH_SHORT).show();
                submitPresenceProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void success(HashMap r) {
                Toast.makeText(NfcActivity.this, "Presence submitted!", Toast.LENGTH_SHORT).show();
                requestActionAnalytics("SubmitPresence", approach);
                saveMessageReport("Presence submitted", ParseUser.getCurrentUser().getUsername());
                submitPresenceProgressBar.setVisibility(View.GONE);
            }
        }, mDoorDbName);
    }

    /**
     * Logs the visitor's presence. Data is stored in Shared Preferences and is used in reports
     * section.
     *
     * @param owner name of the owner of the door
     */
    public void saveDoorVisit(String owner) {
        DateTime dt = new DateTime();
        Door door = new Door(mDoorDbName, dt.getMillis(), null, owner);
        Preferences.addDoor(getApplicationContext(), door);
    }

    /**
     * Logs the message sent to or received from the door. Data is stored in Shared Preferences and
     * is used in reports section.
     *
     * @param messageBody message content
     * @param sender      name of the sender of the message
     */
    public void saveMessageReport(String messageBody, String sender) {
        DateTime dt = new DateTime();

        Message message = new Message(messageBody, dt.getMillis(), null, sender, mDoorDbName);
        Preferences.addMessage(getApplicationContext(), message);
    }

    /**
     * Fetches the related message from server. Door and user identity are sent to the cloud and the
     * result will asynchronously gets back to the device.
     */
    private void getMessageFromServer() {
        progressBar.setVisibility(View.VISIBLE);
        progressBar.setIndeterminate(true);
        if (!isNetworkConnected()) {
            nfcTitle.setText("please check your internet connection and try again...");
            return;
        }
        nfcTitle.setText("Getting access information...");
        requestActionAnalytics("GetMessage", approach);

        ParseApiAdapter.getProfessorMessage(new ParseApiAdapter.Callback() {
            @Override
            public void error(ParseException e) {
                nfcTitle.setText("Error Occurred");
                tagInfo.setText("Server Error: " + e);
                tagInfo.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                btnSubmitPresence.setVisibility(View.VISIBLE);
            }

            @Override
            public void success(HashMap object) {
                nfcTitle.setText("Message Received");
                tagInfo.setText("" + object.get(CLOUD_PARAMETER_MESSAGE));
                tagInfo.setVisibility(View.VISIBLE);
                btnSubmitPresence.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                if ((boolean) object.get("canReply")) btnReplyMessage.setVisibility(View.VISIBLE);
                saveDoorVisit((String) object.get("owner"));
                Debug.Log("Owner", "" + object.get("owner"));
                saveMessageReport("" + object.get(CLOUD_PARAMETER_MESSAGE), "" + object.get("owner"));
            }
        }, mDoorDbName);
    }

    /**
     * Triggers when the injected button is clicked. It process the information and prepare the
     * message to be sent to the cloud
     *
     * @param view button to be clicked
     */
    @OnClick(R.id.btnReplyMessage)
    public void submitReply(View view) {
        Log.d(TAG, "button clicked, the string: " + btnReplyMessage.getText());
        if (!isInSubmitMode) {
            Log.d(TAG, "In the reply mode");
            btnReplyMessage.setText(R.string.submit_message);
            replyMessageText.setVisibility(View.VISIBLE);
            isInSubmitMode = true;
        } else {
            Log.d(TAG, "In the submit mode");
            String replyText = replyMessageText.getText().toString();
            if (replyText == null || replyText.length() == 0) {
                Toast.makeText(this, R.string.submit_message_no_input_inserted_error, Toast.LENGTH_SHORT)
                        .show();
            } else {
                btnReplyMessage.setText(R.string.reply_message);
                replyMessageText.setVisibility(View.GONE);
                Log.d(TAG, replyText);
                isInSubmitMode = false;
                sendReplyMessage(replyText);
            }
        }
    }

    /**
     * Send a message directly to the server as a reply to the door's message
     *
     * @param replyText plain text of the reply message
     */
    private void sendReplyMessage(final String replyText) {
        submitPresenceProgressBar.setVisibility(View.VISIBLE);

        ParseApiAdapter.submitReply(new ParseApiAdapter.Callback() {
            @Override
            public void error(ParseException e) {
                Toast.makeText(NfcActivity.this, "Error occurred, try again..." + e, Toast.LENGTH_SHORT)
                        .show();
                submitPresenceProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void success(HashMap r) {
                Toast.makeText(NfcActivity.this, R.string.reply_message_submitted_successfully,
                        Toast.LENGTH_SHORT).show();
                requestActionAnalytics("ReplyToMessage", approach);
                saveMessageReport(replyText, ParseUser.getCurrentUser().getUsername());
                submitPresenceProgressBar.setVisibility(View.GONE);
            }
        }, mDoorDbName, replyText);
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Debug.Log("NFC Intent", "Discovered tag with intent: " + intent);
        tagInfo.setVisibility(View.GONE);
        btnSubmitPresence.setVisibility(View.GONE);
        submitPresenceProgressBar.setVisibility(View.GONE);

        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

        android.nfc.tech.IsoDep iso = android.nfc.tech.IsoDep.get(tag);
        android.nfc.tech.Ndef ndef = android.nfc.tech.Ndef.get(tag);
        android.nfc.tech.MifareUltralight mifare = android.nfc.tech.MifareUltralight.get(tag);
        android.nfc.tech.NfcA nfca = android.nfc.tech.NfcA.get(tag);

        mDoorUID = bin2hex(tag.getId());
        Log.d(TAG, "UID: " + mDoorUID);
        if (ndef != null) readNdefTag(tag);
        //if (iso != null) readIsoTag(tag);
        //if (nfca != null) readNfcATag(tag);
    }

    static String bin2hex(byte[] data) {
        return String.format("%0" + (data.length * 2) + "X", new BigInteger(1, data));
    }

    private static boolean isAfter(DateTime currentTime, long dayFourDigitFormatted) {
        int hour = (int) dayFourDigitFormatted / 100;
        int minute = (int) dayFourDigitFormatted % 100;

        return currentTime.getHourOfDay() > hour || (currentTime.getHourOfDay() == hour
                && currentTime.getMinuteOfHour() >= minute);
    }

    public void requestActionAnalytics(String action, String approach) {
        DateTime time = DateTime.now();
        DateTimeFormatter fmt = DateTimeFormat.forPattern("E");
        String strEnglish = fmt.print(time);
        String strWhereverUR = fmt.withLocale(Locale.getDefault()).print(time);

        boolean isAfterSunrise = isAfter(time, 700);
        boolean isAfterNoon = isAfter(time, 1200); // 12:00 => noon
        boolean isAfterSunset = isAfter(time, 1700);
        String dayTime = "";

        if (isAfterSunrise && !isAfterNoon) {
            dayTime = "Morning";
        } else if (isAfterNoon && !isAfterSunset) {
            dayTime = "Afternoon";
        } else {
            dayTime = "Night";
        }

        Map<String, String> dimensions = new HashMap<String, String>();
        dimensions.put("weekday", strWhereverUR);
        dimensions.put("dayTime", dayTime);
        dimensions.put("approach", approach);
        dimensions.put("user", ParseUser.getCurrentUser().getEmail());

        ParseAnalytics.trackEventInBackground(action, dimensions);
    }

    public void readNdefTag(Tag tag) {
        android.nfc.tech.Ndef ndef = android.nfc.tech.Ndef.get(tag);
        try {
            ndef.connect();
            boolean readOnly = ndef.canMakeReadOnly();
            Debug.Log(TAG, "makeReadOnly: " + readOnly + "\nmessage: " + readTextFromNdefRecord(
                    ndef.getNdefMessage().getRecords()[0]) +
                    "\nisWritable: " + ndef.isWritable() + "\n");

            mDoorDbName = readTextFromNdefRecord(ndef.getNdefMessage().getRecords()[0]);
            getMessageFromServer();
            ndef.close();
        } catch (FormatException e) {
            e.printStackTrace();
            Log.e(TAG, "FormatException error");
        } catch (IOException e) {
            e.printStackTrace();
            finish();
        } catch (NullPointerException e) {
            e.printStackTrace();
            Toast.makeText(this, "no record available(approach the tag properly?)", Toast.LENGTH_LONG)
                    .show();
            finish();
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null;
    }

    /**
     * reads the payload of the record and turn it into text See NFC forum specification for "Text
     * Record Type Definition" at 3.2.1
     * <p/>
     * http://www.nfc-forum.org/specs/
     * <p/>
     * bit_7 defines encoding bit_6 reserved for future use, must be 0 bit_5..0 length of IANA
     * language code
     */
    private String readTextFromNdefRecord(NdefRecord record) throws UnsupportedEncodingException {
        byte[] payload = record.getPayload();

        // Get the Text Encoding
        String textEncoding =
                ((payload[0] & 128) == 0) ? getString(R.string.UTF8) : getString(R.string.UTF16);

        // Get the Language Code
        int languageCodeLength = payload[0] & 0063;

        // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");
        // e.g. "en"

        // Get the Text
        return new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1,
                textEncoding);
    }

    //
    //  /**
    //   * Following methods are going to be used for future
    //   */
    //
    //  public void formatTag(Tag tag) {
    //    NdefFormatable formatable = NdefFormatable.get(tag);
    //
    //    if (formatable != null) {
    //      try {
    //        formatable.connect();
    //
    //        try {
    //          //NdefRecord[] records = { NdefRecord.createTextRecord("","") };
    //          //NdefMessage message = new NdefMessage(records);
    //          //formatable.format(message);
    //        } catch (Exception e) {
    //          // let the user know the tag refused to format
    //
    //        }
    //      } catch (Exception e) {
    //        // let the user know the tag refused to connect
    //      } finally {
    //        try {
    //          formatable.close();
    //        } catch (IOException e) {
    //          e.printStackTrace();
    //        }
    //      }
    //    } else {
    //      // let the user know the tag cannot be formatted
    //    }
    //  }
    //
    //  public void readIsoTag(Tag tag) {
    //    Debug.Log(TAG, "ISO TAG DETECTED...");
    //    android.nfc.tech.IsoDep iso = android.nfc.tech.IsoDep.get(tag);
    //    try {
    //      byte[] payload = iso.getHistoricalBytes();
    //      Debug.Log(TAG,
    //          "Historical: " + new String(payload) + "\nHiLayerResponse: " + iso.getHiLayerResponse()
    //              + "\n");
    //
    //      iso.close();
    //    } catch (IOException e) {
    //      Debug.Log(TAG, "IOException while reading Iso message..." + e);
    //    }
    //  }
    //
    //  public void readNfcATag(Tag tag) {
    //    Log.d(TAG, "NFCA TAG DETECTED...");
    //    android.nfc.tech.NfcA nfcA = android.nfc.tech.NfcA.get(tag);
    //
    //    try {
    //      nfcA.connect();
    //      byte[] payload = nfcA.getAtqa();
    //      short SAK = nfcA.getSak();
    //      Debug.Log(TAG, "Atqa: " + new String(payload) + "\nSAK: " + SAK + "\n");
    //      nfcA.close();
    //    } catch (IOException e) {
    //      Debug.Log(TAG, "IOException while reading nfcA essage..." + e);
    //    }
    //  }
}