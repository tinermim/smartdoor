package ir.hcilab.smartDoor.server.parse;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import java.util.HashMap;

/**
 * This class handles communication between Parse server and the application.
 * It is specifically implemented for Parse server. If the server is changed, you just
 * need to change this class. Other classes will not be affected.
 */
public class ParseApiAdapter {
  public interface Callback {
    void error(ParseException e);

    void success(HashMap r);
  }

  private static final String CLOUD_FUNC_GET_PROFESSOR_MESSAGE = "getProfessorMessage";
  private static final String CLOUD_FUNC_SUBMIT_REPLY = "submitReply";
  private static final String CLOUD_FUNC_SUBMIT_PRESENCE = "submitPresence";

  private static final String CLOUD_PARAMETER_DOOR_ID = "door_id";
  private static final String CLOUD_PARAMETER_MESSAGE = "message";

  /**
   * Calls a specific cloud function in Parse server. It is working in background and the result
   * will be returned to the observer using {@link Callback}
   *
   * @param c the callback
   * @param functionName equal to the name of cloud function which is implemented in server.
   * The function name is not confidential. All requests need both App key and Database key.
   * @param mDoorDbName name of the door that the student visited
   * @param params sent to the server
   */
  private static void callCloudFunction(final Callback c, String functionName, String mDoorDbName,
      HashMap params) {

    ParseCloud.callFunctionInBackground(functionName, params, new FunctionCallback<HashMap>() {
      public void done(HashMap object, ParseException e) {
        if (e == null) {
          c.success(object);
        } else {
          c.error(e);
        }
      }
    });
  }

  public static void submitPresence(final Callback c, String mDoorDbName) {
    final HashMap<String, Object> params = new HashMap<>();
    params.put(CLOUD_PARAMETER_DOOR_ID, mDoorDbName);
    callCloudFunction(c, CLOUD_FUNC_SUBMIT_PRESENCE, mDoorDbName, params);
  }

  public static void getProfessorMessage(final Callback c, String mDoorDbName) {
    final HashMap<String, Object> params = new HashMap<>();
    params.put(CLOUD_PARAMETER_DOOR_ID, mDoorDbName);
    callCloudFunction(c, CLOUD_FUNC_GET_PROFESSOR_MESSAGE, mDoorDbName, params);
  }

  public static void submitReply(final Callback c, String mDoorDbName, String replyText) {
    final HashMap<String, Object> params = new HashMap<String, Object>();
    params.put(CLOUD_PARAMETER_DOOR_ID, mDoorDbName);
    params.put(CLOUD_PARAMETER_MESSAGE, replyText);
    callCloudFunction(c, CLOUD_FUNC_SUBMIT_REPLY, mDoorDbName, params);
  }
}