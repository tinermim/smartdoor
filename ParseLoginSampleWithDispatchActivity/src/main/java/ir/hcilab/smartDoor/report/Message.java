package ir.hcilab.smartDoor.report;

/**
 * Created by ramtin on 11/23/15 AD.
 */
public class Message {
  public String text;
  public long date;
  public String sender;
  public String color;
  public String doorName;

  public Message(String text, long date, String color, String sender, String doorName) {
    this.text = text;
    this.date = date;
    this.color = color;
    this.sender = sender;
    this.doorName = doorName;
  }

  @Override public boolean equals(Object o) {
    if (o == null) return false;
    if (!o.getClass().equals(Message.class)) return false;

    Message object = (Message) o;
    if (object.text.equals(text) && object.sender.equals(sender) && object.doorName.equals(doorName) &&
        object.date == date) return true;
    return false;
  }
}
