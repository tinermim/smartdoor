package ir.hcilab.smartDoor.report;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.Bind;
import com.parse.ParseUser;
import ir.hcilab.smartDoor.R;
import ir.hcilab.smartDoor.report.core.BaseRecyclerAdapter;
import java.util.Date;
import org.ocpsoft.prettytime.PrettyTime;

public class MessageRecyclerAdapter extends BaseRecyclerAdapter<Message> {

  private String owner;

  @Override public ThingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view;
    if (viewType == 0)
      view = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.list_message_item, parent, false);
    else view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.list_message_item_me, parent, false);

    return new ThingHolder(view);
  }

  public void setOwner(String owner){
      this.owner = owner;
  }

  @Override public int getItemViewType(int position) {
    Message m = items.get(position);
    if(m.sender.equals(ParseUser.getCurrentUser().getUsername()))
      return 1;

    return 0;
  }

  public class ThingHolder extends ViewHolder {
    @Bind(R.id.body) TextView titleTextView;
    @Bind(R.id.date) TextView dateTextView;

    public ThingHolder(View itemView) {
      super(itemView);
    }

    public void populate(Message item) {
      titleTextView.setText(item.text);
      PrettyTime p = new PrettyTime();
      dateTextView.setText(p.format(new Date(item.date)));
    }
  }
}
