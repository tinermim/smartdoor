package ir.hcilab.smartDoor.report;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.Bind;
import ir.hcilab.smartDoor.R;
import ir.hcilab.smartDoor.report.core.BaseRecyclerAdapter;
import java.util.Date;
import org.ocpsoft.prettytime.PrettyTime;

public class DoorRecyclerAdapter extends BaseRecyclerAdapter<Door> {

  @Override public ThingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
    return new ThingHolder(view);
  }

  public class ThingHolder extends BaseRecyclerAdapter<Door>.ViewHolder {
    @Bind(R.id.title) TextView titleTextView;

    @Bind(R.id.date) TextView dateTextView;

    public ThingHolder(View itemView) {
      super(itemView);
    }

    public void populate(Door item) {
      titleTextView.setText(item.text);
      PrettyTime p = new PrettyTime();
      dateTextView.setText("last Visit: " + p.format(new Date(item.date)));
    }
  }
}
