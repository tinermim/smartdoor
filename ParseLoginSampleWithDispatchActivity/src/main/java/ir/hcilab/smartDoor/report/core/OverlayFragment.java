package ir.hcilab.smartDoor.report.core;


import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.balysv.materialmenu.MaterialMenuDrawable;
import ir.hcilab.smartDoor.R;

public class OverlayFragment extends TransitionHelper.BaseFragment {

    @Bind(R.id.overlay) RelativeLayout overlayLayout;
    @Bind(R.id.text_view) TextView textView;

    public OverlayFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_overaly, container, false);
        ButterKnife.bind(this, rootView);
        initBodyText();
        return rootView;
    }

    private void initBodyText() {
        textView.setText("v1.0.0");
        textView.setAlpha(0);
        textView.setTranslationY(100);
        new Handler().postDelayed(new Runnable(){
            public void run() {
                textView.animate()
                        .alpha(1)
                        .setStartDelay(Navigator.ANIM_DURATION/3)
                        .setDuration(Navigator.ANIM_DURATION*5)
                        .setInterpolator(new DecelerateInterpolator(9))
                        .translationY(0)
                        .start();
            }
        }, 200);
    }

    @Override
    public void onBeforeEnter(View contentView) {
        overlayLayout.setVisibility(View.INVISIBLE);
        BaseActivity.of(getActivity()).setHomeIcon(MaterialMenuDrawable.IconState.BURGER);
        BaseActivity.of(getActivity()).animateHomeIcon(MaterialMenuDrawable.IconState.ARROW);
    }

    @Override
    public void onAfterEnter() {

    }

    @Override
    public boolean onBeforeBack() {
        BaseActivity.of(getActivity()).animateHomeIcon(MaterialMenuDrawable.IconState.BURGER);
        return false;
    }

    @Override
    public void onBeforeViewShows(View contentView) {
        TransitionHelper.excludeEnterTarget(getActivity(), R.id.toolbar_container, true);
        TransitionHelper.excludeEnterTarget(getActivity(), R.id.full_screen, true);
        TransitionHelper.excludeEnterTarget(getActivity(), R.id.overlay, true);
    }

//    @TargetApi(21)
//    private void initSharedElementTransition() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            overlayLayout.setVisibility(View.INVISIBLE);
//            ViewCompat.setTransitionName(getBaseActivity().findViewById(R.id.fab), "fab");
//            getActivity().getWindow().getSharedElementEnterTransition().setDuration(Navigator.ANIM_DURATION);
//            getActivity().getWindow().getSharedElementEnterTransition().addListener(new Transition.TransitionListener() {
//                @Override
//                public void onTransitionStart(Transition transition) {
//                    if (overlayLayout.getVisibility() == View.INVISIBLE) {
//                        animateRevealShow(overlayLayout);
//                    } else {
//                        animateRevealHide(overlayLayout);
//                    }
//                }
//
//                @Override
//                public void onTransitionEnd(Transition transition) {
//
//                }
//
//                @Override
//                public void onTransitionCancel(Transition transition) {
//
//                }
//
//                @Override
//                public void onTransitionPause(Transition transition) {
//
//                }
//
//                @Override
//                public void onTransitionResume(Transition transition) {
//
//                }
//            });
//        }
//    }



}
