package ir.hcilab.smartDoor.report;

public class Door {
  public String text;
  public long date;
  public String color;
  public String owner;

  public Door(String text, long date, String color, String owner) {
    this.text = text;
    this.date = date;
    this.color = color;
    this.owner = owner;
  }

  @Override public boolean equals(Object o) {
    if (o == null) return false;
    if (!o.getClass().equals(Door.class)) return false;

    if (((Door) o).text.equals(text)) return true;
    return false;
  }
}
