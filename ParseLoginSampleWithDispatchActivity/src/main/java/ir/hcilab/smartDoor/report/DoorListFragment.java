package ir.hcilab.smartDoor.report;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ir.hcilab.smartDoor.R;
import ir.hcilab.smartDoor.SmartDoorApplication;
import ir.hcilab.smartDoor.report.core.BaseActivity;
import ir.hcilab.smartDoor.report.core.BaseRecyclerAdapter;
import ir.hcilab.smartDoor.report.core.Navigator;
import ir.hcilab.smartDoor.report.core.TransitionHelper;
import ir.hcilab.smartDoor.utils.Preferences;

public class DoorListFragment extends TransitionHelper.BaseFragment {
  @Bind(R.id.recycler) RecyclerView recyclerView;
  DoorRecyclerAdapter recyclerAdapter;

  public DoorListFragment() {
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_thing_list, container, false);
    ButterKnife.bind(this, rootView);
    initRecyclerView();
    SmartDoorApplication application = (SmartDoorApplication) getActivity().getApplication();
    application.getGoogleAnalyticsTracker().setScreenName("Door List");
    return rootView;
  }

  private void initRecyclerView() {
    recyclerAdapter = new DoorRecyclerAdapter();
    recyclerAdapter.updateList(getThings());
    recyclerAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener<Door>() {
      @Override public void onItemClick(View view, final Door item, boolean isLongClick) {
        if (isLongClick) {
          //BaseActivity.of(getActivity()).animateHomeIcon(MaterialMenuDrawable.IconState.X);
          AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
          alertDialogBuilder.setTitle("Delete door")
              .setMessage("Delete the message thread of " + item.text + "?")
          .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialog, int which) {
                Preferences.deleteDoor(getActivity().getApplicationContext(),  item);
                recyclerAdapter.updateList(getThings());
                recyclerAdapter.notifyDataSetChanged();
            }
          }).setNegativeButton("Cancel", null);
          alertDialogBuilder.show();

        } else {
          Navigator.launchDetail(BaseActivity.of(getActivity()), view, item, recyclerView);
        }
      }
    });

    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    recyclerView.setAdapter(recyclerAdapter);
  }

  @Override public boolean onBeforeBack() {
    BaseActivity activity = BaseActivity.of(getActivity());
    return super.onBeforeBack();
  }

  public List<Door> getThings() {
    return Preferences.getDoors(getActivity().getApplicationContext());
  }
}