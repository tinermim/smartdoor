package ir.hcilab.smartDoor.report;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;

import com.balysv.materialmenu.MaterialMenuDrawable;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ir.hcilab.smartDoor.R;
import ir.hcilab.smartDoor.SmartDoorApplication;
import ir.hcilab.smartDoor.report.core.BaseActivity;
import ir.hcilab.smartDoor.report.core.Navigator;
import ir.hcilab.smartDoor.report.core.TransitionHelper;
import ir.hcilab.smartDoor.utils.Debug;
import ir.hcilab.smartDoor.utils.Preferences;

public class MessageDetailFragment extends TransitionHelper.BaseFragment {

    @Bind(R.id.detail_title) TextView titleTextView;
    @Bind(R.id.recyclerBody) RecyclerView detailBody;
    MessageRecyclerAdapter recyclerAdapter;

    public static MessageDetailFragment create() {
        MessageDetailFragment f = new MessageDetailFragment();
        return f;
    }

    public MessageDetailFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_thing_detail, container, false);
        ButterKnife.bind(this, rootView);
        String itemText = getActivity().getIntent().getStringExtra("item_text");
        String owner = getActivity().getIntent().getStringExtra("owner");
        initRecyclerView(itemText, owner);
        titleTextView.setText(owner);

        initDetailBody();
        SmartDoorApplication application = (SmartDoorApplication) getActivity().getApplication();
        application.getGoogleAnalyticsTracker().setScreenName("Message Thread");
        return rootView;
    }

    private void initRecyclerView(String doorName, String owner) {
        recyclerAdapter = new MessageRecyclerAdapter();
        recyclerAdapter.updateList(getMessages(doorName));
        recyclerAdapter.setOwner(owner);
        detailBody.setLayoutManager(new LinearLayoutManager(getActivity()));
        detailBody.setAdapter(recyclerAdapter);
    }

    private void initDetailBody() {
        detailBody.setAlpha(0);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                detailBody.animate().alpha(1).start();
            }
        }, 500);
    }

    @Override
    public void onBeforeViewShows(View contentView) {

        TransitionHelper.excludeEnterTarget(getActivity(), R.id.toolbar_container, true);
        TransitionHelper.excludeEnterTarget(getActivity(), R.id.full_screen, true);
    }

    @Override
    public void onBeforeEnter(View contentView) {
        BaseActivity.of(getActivity()).fragmentBackround.animate().scaleX(.92f).scaleY(.92f).alpha(.6f).setDuration(
            Navigator.ANIM_DURATION).setInterpolator(new AccelerateInterpolator()).start();
        BaseActivity.of(getActivity()).setHomeIcon(MaterialMenuDrawable.IconState.BURGER);
        BaseActivity.of(getActivity()).animateHomeIcon(MaterialMenuDrawable.IconState.ARROW);
    }

    @Override
    public boolean onBeforeBack() {
        BaseActivity.of(getActivity()).animateHomeIcon(MaterialMenuDrawable.IconState.BURGER);
        BaseActivity.of(getActivity()).fragmentBackround.animate().scaleX(1).scaleY(1).alpha(1).translationY(0).setDuration(Navigator.ANIM_DURATION/4).setInterpolator(
            new DecelerateInterpolator()).start();
        TransitionHelper.fadeThenFinish(detailBody, getActivity());
        return false;
    }

    public List<Message> getMessages(String doorName) {
        Debug.Log("Messages" , "door: "+ doorName+", " + Preferences.getMessages(getActivity().getApplicationContext(), doorName) );
        return Preferences.getMessages(getActivity().getApplicationContext(), doorName);

    }
}
